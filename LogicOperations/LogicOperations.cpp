#include <iostream>
#include <bitset>
#include <string>

using namespace std;

// Function to perform NOT operation
string bitwiseNOT(string bin) {
    for (char& c : bin) {
        c = (c == '0') ? '1' : '0';
    }
    return bin;
}

// Function to perform AND operation
string bitwiseAND(string bin1, string bin2) {
    string result;
    for (int i = 0; i < bin1.length(); ++i) {
        result += (bin1[i] == '1' && bin2[i] == '1') ? '1' : '0';
    }
    return result;
}

// Function to perform OR operation
string bitwiseOR(string bin1, string bin2) {
    string result;
    for (int i = 0; i < bin1.length(); ++i) {
        result += (bin1[i] == '1' || bin2[i] == '1') ? '1' : '0';
    }
    return result;
}

// Function to perform XOR operation
string bitwiseXOR(string bin1, string bin2) {
    string result;
    for (int i = 0; i < bin1.length(); ++i) {
        result += (bin1[i] != bin2[i]) ? '1' : '0';
    }
    return result;
}

int main() {
    string binA = "0110"; // 6 in decimal
    string binB = "1100"; // -4 in decimal

    cout << "A: " << binA << endl;
    cout << "B: " << binB << endl;

    cout << "NOT A: " << bitwiseNOT(binA) << endl;
    cout << "NOT B: " << bitwiseNOT(binB) << endl;

    cout << "A AND B: " << bitwiseAND(binA, binB) << endl;
    cout << "A OR B: " << bitwiseOR(binA, binB) << endl;
    cout << "A XOR B: " << bitwiseXOR(binA, binB) << endl;

    return 0;
}
